import subprocess
import os
import numpy as np

import tsfresh
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from time import time
from tsfresh import extract_features


def change_quantiles(ql, qh):
    lista = []
    values = np.meshgrid(ql.tolist(), qh.tolist(), [True, False], ["mean", "var", "median"])
    values = np.array(values).T.reshape(-1, 4)
    cnt = 0
    # print(values.shape)
    for lw, hwm, ia, fa in zip(values[:, 0], values[:, 1], values[:, 2], values[:, 3]):
        if float(lw) <= float(hwm):
            cnt += 1
            d = {}
            d["ql"] = lw
            d["qh"] = hwm
            d["isabs"] = ia
            d["f_agg"] = fa
            lista.append(d)
    return lista, cnt


def wavletData(widths, listCoef):
    lista = []
    values = np.meshgrid(widths, listCoef.tolist())
    values = np.array(values).T.reshape(-1, 2)
    cnt = 0
    # print(values)
    for width, lc in zip(values[:, 0], values[:, 1]):
        d = {}
        d["widths"] = tuple(widths)
        d["w"] = width
        d["coeff"] = lc
        lista.append(d)
    return lista


def fourierData(listCoef):
    lista = []
    values = np.meshgrid(listCoef.tolist(), ["angle", "abs", "real", "imag"])
    values = np.array(values).T.reshape(-1, 2)
    cnt = 0
    # print(values)
    for fcn, lc in zip(values[:, 0], values[:, 1]):
        d = {}
        d["attr"] = lc
        d["coeff"] = int(fcn)
        lista.append(d)
    # print(lista)
    return lista


def mass_index(listQuantiles):
    lista = []
    for x in listQuantiles:
        d = {}
        d["q"] = x
        lista.append(d)
    return lista


def largeStandardDeviation(listRs):
    lista = []
    for x in listRs:
        d = {}
        d["r"] = x
        lista.append(d)
    return lista


def number_mean_crossing(listRs):
    lista = []
    for x in listRs:
        d = {}
        d["r"] = x
        lista.append(d)
    return lista


def number_cwt_peaks(widths):
    lista = []
    for x in listRs:
        d = {}
        d["r"] = x
        lista.append(d)
    return lista


changeQuantiles, b = change_quantiles(np.arange(0.0, 1, 0.15), np.arange(0.0, 1, 0.15))

wavlet_ = wavletData([2, 5, 10, 20, 50], np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))

fourier = fourierData(np.arange(100))

quantiles = mass_index([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])

listRs = largeStandardDeviation(
    [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95])

number_cwt_peaks([])

# data = pd.read_csv("2_Copy_Original_Data/data1.csv")
# data["id"] = np.ones(data.shape[0])
#tData = read_ad()
#tData[0]["id"] = np.ones(tData[0].shape[0])

from tsfresh.feature_extraction import ComprehensiveFCParameters


# settings = ComprehensiveFCParameters()
def set_parameters_extract_features_tsfresh():
    fc_parameters = {
        "abs_energy": None,
        "absolute_sum_of_changes": None,
        "agg_autocorrelation": [
            {"f_agg": "mean", "maxlag": 50},
            {"f_agg": "median", "maxlag": 50},
            {"f_agg": "var", "maxlag": 50},
            {"f_agg": "std", "maxlag": 50}
        ],
        "agg_linear_trend": [{"f_agg": "max", "chunk_len": 2, "attr": "intercept"},
                             {"f_agg": "max", "chunk_len": 2, "attr": "rvalue"},
                             {"f_agg": "max", "chunk_len": 2, "attr": "slope"},
                             {"f_agg": "max", "chunk_len": 2, "attr": "stderr"},
                             {"f_agg": "max", "chunk_len": 5, "attr": "intercept"},
                             {"f_agg": "max", "chunk_len": 5, "attr": "rvalue"},
                             {"f_agg": "max", "chunk_len": 5, "attr": "slope"},
                             {"f_agg": "max", "chunk_len": 5, "attr": "stderr"},
                             {"f_agg": "max", "chunk_len": 10, "attr": "intercept"},
                             {"f_agg": "max", "chunk_len": 10, "attr": "rvalue"},
                             {"f_agg": "max", "chunk_len": 10, "attr": "slope"},
                             {"f_agg": "max", "chunk_len": 10, "attr": "stderr"},
                             {"f_agg": "max", "chunk_len": 20, "attr": "intercept"},
                             {"f_agg": "max", "chunk_len": 20, "attr": "rvalue"},
                             {"f_agg": "max", "chunk_len": 20, "attr": "slope"},
                             {"f_agg": "max", "chunk_len": 20, "attr": "stderr"},
                             {"f_agg": "max", "chunk_len": 50, "attr": "intercept"},
                             {"f_agg": "max", "chunk_len": 50, "attr": "rvalue"},
                             {"f_agg": "max", "chunk_len": 50, "attr": "slope"},
                             {"f_agg": "max", "chunk_len": 50, "attr": "stderr"},
                             {"f_agg": "max", "chunk_len": 100, "attr": "intercept"},
                             {"f_agg": "max", "chunk_len": 100, "attr": "rvalue"},
                             {"f_agg": "max", "chunk_len": 100, "attr": "slope"},
                             {"f_agg": "max", "chunk_len": 100, "attr": "stderr"},

                             {"f_agg": "min", "chunk_len": 2, "attr": "intercept"},
                             {"f_agg": "min", "chunk_len": 2, "attr": "rvalue"},
                             {"f_agg": "min", "chunk_len": 2, "attr": "slope"},
                             {"f_agg": "min", "chunk_len": 2, "attr": "stderr"},
                             {"f_agg": "min", "chunk_len": 5, "attr": "intercept"},
                             {"f_agg": "min", "chunk_len": 5, "attr": "rvalue"},
                             {"f_agg": "min", "chunk_len": 5, "attr": "slope"},
                             {"f_agg": "min", "chunk_len": 5, "attr": "stderr"},
                             {"f_agg": "min", "chunk_len": 10, "attr": "intercept"},
                             {"f_agg": "min", "chunk_len": 10, "attr": "rvalue"},
                             {"f_agg": "min", "chunk_len": 10, "attr": "slope"},
                             {"f_agg": "min", "chunk_len": 10, "attr": "stderr"},
                             {"f_agg": "min", "chunk_len": 20, "attr": "intercept"},
                             {"f_agg": "min", "chunk_len": 20, "attr": "rvalue"},
                             {"f_agg": "min", "chunk_len": 20, "attr": "slope"},
                             {"f_agg": "min", "chunk_len": 20, "attr": "stderr"},
                             {"f_agg": "min", "chunk_len": 50, "attr": "intercept"},
                             {"f_agg": "min", "chunk_len": 50, "attr": "rvalue"},
                             {"f_agg": "min", "chunk_len": 50, "attr": "slope"},
                             {"f_agg": "min", "chunk_len": 50, "attr": "stderr"},
                             {"f_agg": "min", "chunk_len": 100, "attr": "intercept"},
                             {"f_agg": "min", "chunk_len": 100, "attr": "rvalue"},
                             {"f_agg": "min", "chunk_len": 100, "attr": "slope"},
                             {"f_agg": "min", "chunk_len": 100, "attr": "stderr"},

                             {"f_agg": "mean", "chunk_len": 2, "attr": "intercept"},
                             {"f_agg": "mean", "chunk_len": 2, "attr": "rvalue"},
                             {"f_agg": "mean", "chunk_len": 2, "attr": "slope"},
                             {"f_agg": "mean", "chunk_len": 2, "attr": "stderr"},
                             {"f_agg": "mean", "chunk_len": 5, "attr": "intercept"},
                             {"f_agg": "mean", "chunk_len": 5, "attr": "rvalue"},
                             {"f_agg": "mean", "chunk_len": 5, "attr": "slope"},
                             {"f_agg": "mean", "chunk_len": 5, "attr": "stderr"},
                             {"f_agg": "mean", "chunk_len": 10, "attr": "intercept"},
                             {"f_agg": "mean", "chunk_len": 10, "attr": "rvalue"},
                             {"f_agg": "mean", "chunk_len": 10, "attr": "slope"},
                             {"f_agg": "mean", "chunk_len": 10, "attr": "stderr"},
                             {"f_agg": "mean", "chunk_len": 20, "attr": "intercept"},
                             {"f_agg": "mean", "chunk_len": 20, "attr": "rvalue"},
                             {"f_agg": "mean", "chunk_len": 20, "attr": "slope"},
                             {"f_agg": "mean", "chunk_len": 20, "attr": "stderr"},
                             {"f_agg": "mean", "chunk_len": 50, "attr": "intercept"},
                             {"f_agg": "mean", "chunk_len": 50, "attr": "rvalue"},
                             {"f_agg": "mean", "chunk_len": 50, "attr": "slope"},
                             {"f_agg": "mean", "chunk_len": 50, "attr": "stderr"},
                             {"f_agg": "mean", "chunk_len": 100, "attr": "intercept"},
                             {"f_agg": "mean", "chunk_len": 100, "attr": "rvalue"},
                             {"f_agg": "mean", "chunk_len": 100, "attr": "slope"},
                             {"f_agg": "mean", "chunk_len": 100, "attr": "stderr"},

                             {"f_agg": "var", "chunk_len": 2, "attr": "intercept"},
                             {"f_agg": "var", "chunk_len": 2, "attr": "rvalue"},
                             {"f_agg": "var", "chunk_len": 2, "attr": "slope"},
                             {"f_agg": "var", "chunk_len": 2, "attr": "stderr"},
                             {"f_agg": "var", "chunk_len": 5, "attr": "intercept"},
                             {"f_agg": "var", "chunk_len": 5, "attr": "rvalue"},
                             {"f_agg": "var", "chunk_len": 5, "attr": "slope"},
                             {"f_agg": "var", "chunk_len": 5, "attr": "stderr"},
                             {"f_agg": "var", "chunk_len": 10, "attr": "intercept"},
                             {"f_agg": "var", "chunk_len": 10, "attr": "rvalue"},
                             {"f_agg": "var", "chunk_len": 10, "attr": "slope"},
                             {"f_agg": "var", "chunk_len": 10, "attr": "stderr"},
                             {"f_agg": "var", "chunk_len": 20, "attr": "intercept"},
                             {"f_agg": "var", "chunk_len": 20, "attr": "rvalue"},
                             {"f_agg": "var", "chunk_len": 20, "attr": "slope"},
                             {"f_agg": "var", "chunk_len": 20, "attr": "stderr"},
                             {"f_agg": "var", "chunk_len": 50, "attr": "intercept"},
                             {"f_agg": "var", "chunk_len": 50, "attr": "rvalue"},
                             {"f_agg": "var", "chunk_len": 50, "attr": "slope"},
                             {"f_agg": "var", "chunk_len": 50, "attr": "stderr"},
                             {"f_agg": "var", "chunk_len": 100, "attr": "intercept"},
                             {"f_agg": "var", "chunk_len": 100, "attr": "rvalue"},
                             {"f_agg": "var", "chunk_len": 100, "attr": "slope"},
                             {"f_agg": "var", "chunk_len": 100, "attr": "stderr"},

                             {"f_agg": "std", "chunk_len": 2, "attr": "intercept"},
                             {"f_agg": "std", "chunk_len": 2, "attr": "rvalue"},
                             {"f_agg": "std", "chunk_len": 2, "attr": "slope"},
                             {"f_agg": "std", "chunk_len": 2, "attr": "stderr"},
                             {"f_agg": "std", "chunk_len": 5, "attr": "intercept"},
                             {"f_agg": "std", "chunk_len": 5, "attr": "rvalue"},
                             {"f_agg": "std", "chunk_len": 5, "attr": "slope"},
                             {"f_agg": "std", "chunk_len": 5, "attr": "stderr"},
                             {"f_agg": "std", "chunk_len": 10, "attr": "intercept"},
                             {"f_agg": "std", "chunk_len": 10, "attr": "rvalue"},
                             {"f_agg": "std", "chunk_len": 10, "attr": "slope"},
                             {"f_agg": "std", "chunk_len": 10, "attr": "stderr"},
                             {"f_agg": "std", "chunk_len": 20, "attr": "intercept"},
                             {"f_agg": "std", "chunk_len": 20, "attr": "rvalue"},
                             {"f_agg": "std", "chunk_len": 20, "attr": "slope"},
                             {"f_agg": "std", "chunk_len": 20, "attr": "stderr"},
                             {"f_agg": "std", "chunk_len": 50, "attr": "intercept"},
                             {"f_agg": "std", "chunk_len": 50, "attr": "rvalue"},
                             {"f_agg": "std", "chunk_len": 50, "attr": "slope"},
                             {"f_agg": "std", "chunk_len": 50, "attr": "stderr"},
                             {"f_agg": "std", "chunk_len": 100, "attr": "intercept"},
                             {"f_agg": "std", "chunk_len": 100, "attr": "rvalue"},
                             {"f_agg": "std", "chunk_len": 100, "attr": "slope"},
                             {"f_agg": "std", "chunk_len": 100, "attr": "stderr"}
                             ],

        "approximate_entropy": [
            {"m": 2, "r": 0.1},
            {"m": 2, "r": 0.2},
            {"m": 2, "r": 0.3},
            {"m": 2, "r": 0.4},
            {"m": 2, "r": 0.5},
            {"m": 2, "r": 0.6},
            {"m": 2, "r": 0.7},
            {"m": 2, "r": 0.8},
            {"m": 2, "r": 0.9},

            {"m": 5, "r": 0.1},
            {"m": 5, "r": 0.12},
            {"m": 5, "r": 0.2},
            {"m": 5, "r": 0.3},
            {"m": 5, "r": 0.15},
            {"m": 5, "r": 0.06},
            {"m": 5, "r": 0.07},
            {"m": 5, "r": 0.8},
            {"m": 5, "r": 0.19},

            {"m": 3, "r": 0.1},
            {"m": 3, "r": 0.2},
            {"m": 3, "r": 0.3},
            {"m": 3, "r": 0.08},
            {"m": 3, "r": 0.05},
            {"m": 3, "r": 0.16},
        ],
        "ar_coefficient": [

            {"coeff": 0, "k": 5},
            {"coeff": 1, "k": 5},
            {"coeff": 2, "k": 5},
            {"coeff": 3, "k": 5},
            {"coeff": 4, "k": 5},
            {"coeff": 5, "k": 5},

            {"coeff": 0, "k": 10},
            {"coeff": 1, "k": 10},
            {"coeff": 2, "k": 10},
            {"coeff": 3, "k": 10},
            {"coeff": 4, "k": 10},
            {"coeff": 5, "k": 10},
            {"coeff": 6, "k": 10},
            {"coeff": 7, "k": 10},
            {"coeff": 8, "k": 10},
            {"coeff": 9, "k": 10},
            {"coeff": 10, "k": 10},

            {"coeff": 0, "k": 20},
            {"coeff": 1, "k": 20},
            {"coeff": 2, "k": 20},
            {"coeff": 3, "k": 20},
            {"coeff": 4, "k": 20},
            {"coeff": 5, "k": 20},
            {"coeff": 6, "k": 20},
            {"coeff": 7, "k": 20},
            {"coeff": 8, "k": 20},
            {"coeff": 9, "k": 20},
            {"coeff": 10, "k": 20},
            {"coeff": 11, "k": 20},
            {"coeff": 12, "k": 20},
            {"coeff": 13, "k": 20},
            {"coeff": 14, "k": 20},
            {"coeff": 15, "k": 20},
            {"coeff": 16, "k": 20},
            {"coeff": 17, "k": 20},
            {"coeff": 18, "k": 20},
            {"coeff": 19, "k": 20},
            {"coeff": 20, "k": 20},

        ],
        "augmented_dickey_fuller": [
            {"attr": "teststat"},
            {"attr": "pvalue"},
            {"attr": "usedlag"},
        ],
        "autocorrelation": [
            {"lag": 1},
            {"lag": 2},
            {"lag": 5},
            {"lag": 10},
            {"lag": 20},
            {"lag": 50},
            {"lag": 100},
        ],
        "binned_entropy": [
            {"max_bins": 2},
            {"max_bins": 5},
            {"max_bins": 10},
            {"max_bins": 20},
            {"max_bins": 50},
            {"max_bins": 100},
        ],

        "c3": [
            {"lag": 2},
            {"lag": 5},
            {"lag": 8},
            {"lag": 10},
            {"lag": 20},
            {"lag": 25},
            {"lag": 50},

        ],

        "change_quantiles": changeQuantiles,
        "count_above_mean": None,
        "count_below_mean": None,
        "cid_ce": [{"normalize": True}, {"normalize": False}],

        "cwt_coefficients": wavlet_,
        "fft_aggregated": [{"aggtype": "centroid"},
                           {"aggtype": "variance"},
                           {"aggtype": "skew"},
                           {"aggtype": "kurtosis"}],
        "fft_coefficient": fourier,
        "first_location_of_maximum": None,
        "first_location_of_minimum": None,
        "friedrich_coefficients": [{"m": 3, "r": 30, "coeff": 0},
                                   {"m": 3, "r": 30, "coeff": 1},
                                   {"m": 3, "r": 30, "coeff": 2},
                                   {"m": 3, "r": 30, "coeff": 3},
                                   ],  # I do not understand this feature
        "has_duplicate": None,  # probably  won't use this
        "has_duplicate_max": None,  # probably  won't use this
        "has_duplicate_min": None,  # probably  won't use this
        "index_mass_quantile": quantiles,
        "kurtosis": None,
        "large_standard_deviation": listRs,
    # If using this feature it should be converted into bool value if the value for r in range(0. 0.25) are 1's, else it is zero
        "last_location_of_maximum": None,  # this won't be used
        "last_location_of_minimum": None,  # this won't be used
        "length": None,  # this won't be used
        "linear_trend": [{"attr": "intercept"},
                         {"attr": "pvalue"},
                         {"attr": "rvalue"},
                         {"attr": "slope"},
                         {"attr": "stderr"}],
        "longest_strike_above_mean": None,
        "longest_strike_below_mean": None,
        # HERE I SHOULD PUT A FEATURE OF NUMBER OF CROSSING FOR THE MEAN (IT WILL SHOW HOW DYNAMIC THE SYSTEM IS AND HOW DRASTIC IS THAT DYNAMIC)

        "max_langevin_fixed_point": [{"m": 3, "r": 30}],  # I DO NOT UNDERSTAND THIS FEATURE
        "maximum": None,
        "mean": None,
        "mean_abs_change": None,
        "mean_change": None,
        "mean_second_derivative_central": None,
        "median": None,
        "minimum": None,
        "number_crossing_m": [
            {"m": 1.381503},  # number of crossing mean
            {"m": 2.269628},  # number of crossing mean+std
            {"m": 0.49337}  # number of crossing mean-std
        ],
        "standard_deviation": None,

        "number_cwt_peaks": [
            {"n": 2},
            {"n": 5},
            {"n": 10},
            {"n": 20},
            {"n": 50}
        ],

        "number_peaks": [
            {"n": 5},  # local peak
            {"n": 15},  # locally peaked
            {"n": 30},
            {"n": 50},  # strong peak
            {"n": 200},  # the strongest peak
        ],

        "partial_autocorrelation": [
            {"lag": 0},
            {"lag": 1},
            {"lag": 2},
            {"lag": 3},
            {"lag": 4},
            {"lag": 5},
            {"lag": 6},
            {"lag": 7},
            {"lag": 8},
            {"lag": 9},
            {"lag": 10},
            {"lag": 11},
            {"lag": 12},
            {"lag": 13},
            {"lag": 14},
            {"lag": 15},
            {"lag": 16},
            {"lag": 17},
            {"lag": 18},
            {"lag": 19},
            {"lag": 20},
        ],

        "percentage_of_reoccurring_datapoints_to_all_datapoints": None,  # This won't be used
        "percentage_of_reoccurring_values_to_all_values": None,  # This won't be used
        "quantile": [
            {"q": 0.1},
            {"q": 0.2},
            {"q": 0.25},
            {"q": 0.3},
            {"q": 0.35},
            {"q": 0.4},
            {"q": 0.5},
            {"q": 0.6},
            {"q": 0.7},
            {"q": 0.75},
            {"q": 0.8},
            {"q": 0.9},
            {"q": 0.95},
        ],  # probably will not be used

        "range_count": [
            {"min": 0.49337, "max": 2.269628},  # mean(x) +/- 1*std(x)
            {"min": -0.394747, "max": 3.157753},
            {"min": 0.93744, "max": 1.8255655}
        ],  # probably won't use this feature

        "ratio_beyond_r_sigma": [
            {"r": 0.5},
            {"r": 1},
            {"r": 1.25},
            {"r": 1.5},
            {"r": 2},
            {"r": 0.25},

        ],  # this feature gives indication about the distribution of the values

        "ratio_value_number_to_time_series_length": None,  # This feature probably will not be used
        "sample_entropy": None,
        "skewness": None,
        "spkt_welch_density": [
            {"coeff": 0},
            {"coeff": 1},
            {"coeff": 2},
            {"coeff": 3},
            {"coeff": 4},
            {"coeff": 5},
            {"coeff": 6},
            {"coeff": 7},
            {"coeff": 8},
        ],

        "sum_of_reoccurring_data_points": None,  # this won't be used
        "sum_of_reoccurring_values": None,  # This won't be used
        "sum_values": None,  # USE THIS OR ABS ENERGY

        "symmetry_looking": [
            {"r": 0.0},
            {"r": 0.05},
            {"r": 0.1},
            {"r": 0.15},
            {"r": 0.2},
            {"r": 0.25},
            {"r": 0.3},
            {"r": 0.35},
            {"r": 0.4},
            {"r": 0.45},
            {"r": 0.5},
            {"r": 0.55},
            {"r": 0.6},
            {"r": 0.65},
            {"r": 0.7},
            {"r": 0.75},
            {"r": 0.8},
            {"r": 0.85},
            {"r": 0.9},
            {"r": 0.95},
        ],

        "time_reversal_asymmetry_statistic": [
            {"lag": 1},
            {"lag": 2},
            {"lag": 3},
            {"lag": 4},
            {"lag": 5},
        ],  # This will not be used

        "value_count": [{"value": 0}],  # This feature won't be used

        "variance": None,
        "variance_larger_than_standard_deviation": None  # This feature won't be used

    }
    return fc_parameters

# from tsfresh.feature_extraction import extract_features

# q = extract_features(tData[0].loc[:, ["id", "time", "h2"]], column_id="id", column_sort='time', column_value="h2", default_fc_parameters=fc_parameters)
# q.T

fc_parameters= set_parameters_extract_features_tsfresh()

with open("Files.txt", "r") as patients:
    contentPatients = patients.readlines()
    contentPatients = contentPatients[0].rsplit("\t")
    filePath = "2_Copy_Original_Data/Processed_Files/"
    toBuild = []
    for folder in contentPatients:
        pati = filePath + folder + "/" + folder + "_chan_0"
        signalDataFiles = os.listdir(pati)
        for file in signalDataFiles:
            signalPath = pati +"/" + file
            with open(signalPath, "r") as sig0File:
                sigValues = sig0File.readlines()
                numpyArraySigValues = np.array(sigValues[:-1]).astype("float64")
                target = sigValues[-1]
                timeVec = np.arange(0, np.shape(numpyArraySigValues)[0])
                id = np.full(shape=numpyArraySigValues.shape[0], fill_value=1)
                toBuild.append((pd.DataFrame({"timeIndex": timeVec, "signalValues":numpyArraySigValues, "id":id}), target, file))

#extract_features(toBuild[0][0].loc[:, [ "id", "timeIndex", "signalValues"]], column_id="id", column_sort='timeIndex', column_value="signalValues", default_fc_parameters=fc_parameters)

startTime = time()
for x in range(len(toBuild)):
    a = extract_features(toBuild[x][0].loc[:, [ "id", "timeIndex", "signalValues"]], column_id="id", column_sort='timeIndex', column_value="signalValues", default_fc_parameters=fc_parameters)
print("Execution time: ", time()-startTime)