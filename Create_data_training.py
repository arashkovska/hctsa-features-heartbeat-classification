import scipy.io as spio
import os
import pandas as pd
import numpy as np



def RemoveWrongColumns(CreateDataQ):
    """
    :param CreateDataQ: Quality Matrix of HCTSA
    :return: list of indecies that should be contained by the data
    """
    listIndex = []
    for x in range(CreateDataQ.shape[1]):
        if CreateDataQ.iloc[:, x].sum() == 0:
            listIndex.append(x)
    return listIndex

dataPath = r"3_Preprocessed_Data/Proba"
folders = os.listdir(dataPath)
listaP = []
listaQ = []
listTarget = []
cnt = 1
for x in folders:
    data = pd.DataFrame(spio.loadmat(dataPath + "/" + x + '/' + 'HCTSA.mat')["TS_DataMat"])
    quality  = pd.DataFrame(spio.loadmat(dataPath + "/" + x + '/' + 'HCTSA.mat')["TS_Quality"])
    target = pd.read_csv(dataPath + "/" + x + '/' + 'table.txt')
    data["Name"] = target.loc[:,["Name"]]
    data['Keywords'] = target.loc[:,['Keywords']]
    data['ID'] = np.full((data.shape[0],), int(x))
    listaP.append(data)
    listaQ.append(quality)


CreateDataTraining = pd.concat(listaP)
CreateDataQ = pd.concat(listaQ)

featuresToPerserve = RemoveWrongColumns(CreateDataQ)
featuresToPerserve.append("Name")
featuresToPerserve.append("Keywords")
featuresToPerserve.append("ID")

finalFrame = CreateDataTraining.loc[:, featuresToPerserve]
finalFrame.to_csv('TrainingSet.csv', index=False)