import os
path = "2_Copy_Original_Data/Matlab/"
readPath = "2_Copy_Original_Data/Processed_Files/"

with open("Files.txt", "r") as patients:
    files = patients.readlines()
    files = files[0].rsplit("\t")
    for file in files:
        subFolderName = readPath + file + "/" + file
        subFolderName1 = path + file + "/" + file
        beatsCh0 = os.listdir(subFolderName + "_chan_0")
        beatsCh1 = os.listdir(subFolderName + "_chan_1")
        print(file)
        nString = ''
        try:
            os.makedirs(subFolderName1 + "_chan_0")
            os.makedirs(subFolderName1 + "_chan_1")
        except:
            pass

        for subfile in beatsCh0:

            ffname = subFolderName + "_chan_0/" + subfile
            with open(ffname, "r") as signalFile:
                contentSignal =  signalFile.readlines()
                valuesSingalString = ""
                for value in contentSignal[:-1]:
                    valuesSingalString += value
            toWriteFileName = subFolderName1 + "_chan_0/" + subfile

            nString += subfile + " " + contentSignal[-1] + "\n"
            with open(toWriteFileName, "w") as signalWithoutTargetFile:
                signalWithoutTargetFile.write(valuesSingalString)
            with open(subFolderName1 + "_chanal_0.txt", "w") as matlabFile:
                matlabFile.write(nString)

        nString = ''
        for subfile in beatsCh1:

            ffname = subFolderName + "_chan_1/" + subfile
            with open(ffname, "r") as signalFile:
                contentSignal =  signalFile.readlines()
                valuesSingalString = ""
                for value in contentSignal[:-1]:
                    valuesSingalString += value
                nString += subfile + " " + contentSignal[-1] + "\n"
                toWriteFileName = subFolderName1 + "_chan_1/" + subfile
                with open(toWriteFileName, "w") as signalWithoutTargetFile:
                    signalWithoutTargetFile.write(valuesSingalString)

                with open(subFolderName1 + "_chanal_1.txt", "w") as matlabFile:
                    matlabFile.write(nString)

