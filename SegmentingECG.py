import numpy as np
import os
import matplotlib.pyplot as plt
from time import time

freq = 360   #this is the sampling frequency of the data
timePrior = 0.2 # in seconds
pathToAnnotations = "2_Copy_Original_Data/Processed_Files/"
const = int(timePrior/ (1 / freq))
startTime = time()

def writeToFiles(val, faq, succPeaks, succLabels, chanal):
    try:
        os.mkdir(pathToAnnotations +  faq + "/" + faq + "_" + chanal)
    except:
        pass

    for values in range(len(val)):
        fileName = pathToAnnotations +  faq + "/" + faq + "_" + chanal + "/" + faq + chanal + "_"+ str(succPeaks[values])
        with open(fileName+".dat", "w") as outFile:
            towrite = ''
            for element in val[values]:
                towrite += str(element) + "\n"
            towrite += succLabels[values]
            outFile.write(towrite)

with open("Files.txt", "r") as nnFile:
    fil = nnFile.readlines()
    fil = fil[0].rsplit("\t")
    for faq in fil:
        intervalFile = faq + "intervals.txt"
        signalFile_ch0 = faq + "0.dat"
        signalFile_ch1 = faq + "1.dat"
        folderToWritePath = "2_Copy_Original_Data/Processed_Files/"+faq
        #because the time units are samples we can directly go symmetric arount he measured point
        with open(pathToAnnotations+intervalFile) as interval:
            try:
                os.mkdir(folderToWritePath)
            except:
                pass

            contentInterval = interval.readlines()
            peaks = []
            labels = []
            for peak in contentInterval:
                peakProperties = peak.replace("\n","").rsplit("\t")
                peaks.append(int(peakProperties[0]))
                labels.append(peakProperties[1])
            peaks = np.array(peaks)
            labels = np.array(labels)

        with open(pathToAnnotations+signalFile_ch0, "r") as signal0:
            contentSig0 = signal0.readlines()
            numpySig0 = np.array(contentSig0).astype("float64")
            val = []
            succPeaks =[]
            succLabels = []
            for peak in range(peaks.shape[0]-1):
                if peaks[peak] - const < 0:
                    continue
                else:
                    lowerBound = peaks[peak]-const
                    upperBound = peaks[peak+1]-const
                    val.append(numpySig0[lowerBound:upperBound])
                    succPeaks.append(peaks[peak])
                    succLabels.append(labels[peak])
            val.append(numpySig0[upperBound:])
            succPeaks.append(peaks[-1])
            succLabels.append(labels[-1])

            print("#################\n")
            print("File ", faq)
            print("Val size: ", len(val))
            print("succPeaks size: ", len(peaks))
            #print("succLabels size: ", len(succLabels))

            print("#################\n")
            writeToFiles(val, faq, succPeaks, succLabels, "chan_0")

            with open(pathToAnnotations + signalFile_ch1, "r") as signal1:
                contentSig1 = signal1.readlines()
                numpySig1 = np.array(contentSig1).astype("float64")
                val = []
                succPeaks = []
                succLabels = []
                for peak in range(peaks.shape[0] - 1):
                    if peaks[peak] - const < 0:
                        continue
                    else:
                        lowerBound = peaks[peak] - const
                        upperBound = peaks[peak + 1] - const
                        val.append(numpySig1[lowerBound:upperBound])
                        succPeaks.append(peaks[peak])
                        succLabels.append(labels[peak])
                val.append(numpySig1[upperBound:])
                succPeaks.append(peaks[-1])
                succLabels.append(labels[-1])

                writeToFiles(val, faq, succPeaks, succLabels, "chan_1")

print("Execution time: ", time()-startTime)