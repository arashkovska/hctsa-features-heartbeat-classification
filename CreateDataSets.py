import numpy as np
import matplotlib.pyplot as plt
import os
from time import time


startTime = time()

def plot_RR_interval(signalLine1, startIndex, newIndex):
    plt.figure(startIndex)
    plt.plot(signalLine1[startIndex:newIndex])
    plt.title(string[1].replace("\n", ""))
    plt.show()



with open("Files.txt", "r") as file:
    content = file.readlines()
    content = content[0].rsplit("\t")
    for datFile in content:
        try:
            os.mkdir("2_Copy_Original_Data/Without_targets/" + datFile)
        except:
            pass
        datFileName1 = "2_Copy_Original_Data/Processed_Files/" + datFile + str(0) + ".dat"
        with open(datFileName1, "r") as file:
            signal = file.readlines()
            signalLine1 = np.array(signal).astype("float64")
        datFileName2 = "2_Copy_Original_Data/Processed_Files/" + datFile + str(1) + ".dat"

        with open(datFileName2, "r") as file:
            signal = file.readlines()
            signalLine2 = np.array(signal).astype("float64")

        intervals = "2_Copy_Original_Data/Processed_Files/" + datFile + "intervals.txt"

        print(datFile)

        with open(intervals, "r") as file:
            content = file.readlines()
            startIndex = 0
            for x in content:
                string = x.split("\t")
                newIndex = int(string[0])

                with open(r"2_Copy_Original_Data/Without_targets/" + datFile + "/" + datFile + str(0) + str(newIndex) + "_chanal_0.dat", "w") as file:
                    RR_interval = signalLine1[startIndex:newIndex].astype("str")
                    stringToBeWritten = ""
                    for valueInRR in RR_interval:
                        stringToBeWritten += valueInRR + "\n"
#                    stringToBeWritten += string[1]
                    file.write(stringToBeWritten)

                with open(r"2_Copy_Original_Data/Without_targets/" + datFile + "/" + datFile + str(0) + str(newIndex) + "_chanal_1.dat",  "w") as file:
                    RR_interval = signalLine2[startIndex:newIndex].astype("str")
                    stringToBeWritten = ""
                    for valueInRR in RR_interval:
                        stringToBeWritten += valueInRR + "\n"
#                    stringToBeWritten += string[1]
                    file.write(stringToBeWritten)
                #plot_RR_interval(signalLine1, startIndex, newIndex)


                startIndex = newIndex

print("Execution time: ", time()- startTime)