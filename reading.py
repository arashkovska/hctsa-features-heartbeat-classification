import pandas as pd
import wfdb as wf

#This script is downloading the data and creating separate files for each of the canales in each of the records
#At the end of each of the records there is


"""
def read_signals():
    for file in files:
        name = file.rsplit(".")[0]
        return wfdbio.rdsamp(name, pb_dir='mitdb')

def read_annotations():
    for file in files:
        name = file.rsplit(".")[0]
        return wfdbio.rdann(record_name = name, pb_dir='mitdb', extension="dat", return_label_elements="symbol")
"""
def download_data():
    wf.dl_database(db_dir="mitdb", annotators="all", keep_subdirs=False, dl_dir='1_Original_Data/MIT-BIH')

def plot_data():
    wf.plot_all_records("2_Copy_Original_Data/MIT-BIH")

def properties_of_database(dataBase_name):
    print("The database: {} has the following properties.".format(dataBase_name))
    print("There are total of : {} elements.".format(len(wf.get_record_list(dataBase_name))))
    print("Their names are: ", wf.get_record_list(dataBase_name))

def show_attribute_labels():
    print(wf.show_ann_classes())
    print(wf.show_ann_labels())

def read_record(record_name):
    fileName = '2_Copy_Original_Data/MIT-BIH/' + str(record_name)
    ann = wf.rdann(fileName, 'atr')
    record = wf.rdrecord(fileName)
    return record.p_signal[:, 0], record.p_signal[:, 1], len(record.p_signal[:, 1]), ann.sample, ann.ann_len, ann.symbol, record, ann

def create_dat_files():
    with open("Files.txt", "r") as file:
        content = file.readlines()
        content = content[0].rsplit("\t")
        for datFile in content:
            datFileName1 = "2_Copy_Original_Data/Processed_Files/" + datFile + str(0) + ".dat"
            datFileName2 = "2_Copy_Original_Data/Processed_Files/" + datFile + str(1) + ".dat"
            datFileName3 = "2_Copy_Original_Data/Processed_Files/" + datFile + "intervals.txt"
            toWrite = read_record(datFile)
            print("################################\n")
            print("The record:", datFile)
            print("Read record: ",toWrite[-2].sig_name)
            print("################################\n")
            with open(datFileName1, "w") as file:
                q = toWrite[0].astype("str")
                cc = ""
                for x in q:
                    cc += x + "\n"
                #file.write(cc)
            with open(datFileName2, "w") as file:
                q = toWrite[1].astype("str")
                cc = ""
                for x in q:
                    cc += x + "\n"
                #file.write(cc)
            with open(datFileName3, "w") as file:
                q = toWrite[3].astype("str")
                s = toWrite[5]
                cc = ""
                for x in range(len(q)):
                    cc += q[x] + "\t" + s[x] + "\n"
                #file.write(cc)

#record = wf.rdrecord('2_Copy_Original_Data/MIT-BIH/100')
#ann = wf.rdann('2_Copy_Original_Data/MIT-BIH/100', 'atr')
#wf.plot_items(signal=record.p_signal)