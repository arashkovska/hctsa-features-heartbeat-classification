import os

pathToTargets = "2_Copy_Original_Data/Processed_Files/"
pathToSignals = "2_Copy_Original_Data/Without_targets/"


folders = os.listdir(pathToSignals)
for folder in folders:
    signal = pathToSignals + folder
    filesInFolder = os.listdir(signal)
    toWriteChanal0 = ""
    toWriteChanal1 = ""
    for ff in filesInFolder:
        with open(pathToTargets+folder+"/"+ff, "r") as file1:
            con = file1.readlines()
            target = con[-1]
        if "chanal_1" in ff:
            toWriteChanal1 += ff + "\t" + target +"\n"
        else:
            toWriteChanal0 += ff + "\t" + target +"\n"
    try:
        os.mkdir("2_Copy_Original_Data/Matlab/" + folder)
        os.mkdir("2_Copy_Original_Data/Matlab/" + folder + "/chanal_0")
        os.mkdir("2_Copy_Original_Data/Matlab/" + folder + "/chanal_1")
    except:
        pass

    with open("2_Copy_Original_Data/Matlab/" + folder + "/chanal_0/" +folder +".txt", "w") as ch0_file:
        ch0_file.write(toWriteChanal0)

    with open("2_Copy_Original_Data/Matlab/" + folder + "/chanal_1/" +folder +".txt", "w") as ch1_file:
        ch1_file.write(toWriteChanal1)
