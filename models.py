import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from time import time

startTime = time()

seed = 42
np.random.seed(seed)

filePath = r"/home/jasminb/PycharmProjects/Arrhythmia_Classification" + "/partialTrainingSet_3patients.csv"
data = pd.read_csv(filePath)

d = {"L": 1, "R":1, "N":1, "e":1, "j":1, "A":2, "a":2, "J":2, "S":2, "V":3, "E":3, "F":4, "Q":5 ,"P":5, "f":5, "U":5, "!":6, "+":6, "~":6, "|":6, "[":6, "]":6, "x":6, np.nan:6}
data.Keywords = np.array([d[x] for x in data.Keywords])


data = data[data.loc[:,"Keywords"] != 6].copy()

from sklearn.preprocessing import StandardScaler
sc = StandardScaler()



X = data.iloc[:, :-3].values
#X = sc.fit_transform(X)

y = data.Keywords.values
groups = data.iloc[:, -1].values


"""
#from sklearn.manifold import Isomap
#kpca = Isomap(n_components=2, n_neighbors=50)
#components = kpca.fit_transform(X)
#plt.scatter(components[:, 0], components[:, 1])



from sklearn.model_selection import LeaveOneGroupOut
logo = LeaveOneGroupOut()

logo.get_n_splits(groups=groups)


from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score

from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier

from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import GridSearchCV

rf = RandomForestClassifier(n_estimators=100, random_state=seed, class_weight='balanced', max_features='sqrt')
rf = OneVsRestClassifier(rf, n_jobs=-1)


scoresF1 = []
scoresAcc = []
scoresPrecision = []
scoresRecall = []
scoresROC = []

for train_index, test_index in logo.split(X, y, groups):
    print("Train: ", train_index, "Test: ", test_index)
    X_train, X_test = X[train_index.tolist(), :], X[test_index.tolist(), :]
    y_train, y_test = y[train_index.tolist()], y[test_index.tolist()]

    rf.fit(X_train, y_train)
    print("\nFinished training\n")

    y_pred = rf.predict(X_test)
    print("\nFinished testing\n")

    scoresF1.append(f1_score(y_true=y_test, y_pred=y_pred, average='micro'))
    scoresAcc.append(accuracy_score(y_true=y_test, y_pred=y_pred))


    #scoresPrecision.append(precision_score(y_true=y_test, y_pred=y_pred, average='micro'))
    #scoresRecall.append(recall_score(y_true=y_test, y_pred=y_pred, average='micro'))
    #scoresROC.append(roc_auc_score(y_true=y_test, y_score=y_pred, average='micro'))
"""

from sklearn.model_selection import LeaveOneGroupOut
logo = LeaveOneGroupOut()

from sklearn.metrics import make_scorer
from sklearn.metrics import f1_score


f1 = make_scorer(f1_score, average='micro')

def cv_Workflow(algorithm, data_train1, yTR, name, col, X_train, X_test, groups):

    from sklearn.metrics import accuracy_score
    from sklearn.model_selection import cross_val_score
    from sklearn.model_selection import cross_val_predict
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    scoresAlgorithm = []
    bestParametersPerFold = []
    predictionsAlgorithm = []
    cvResults = []

    print("\nStart traning the {} model\n".format(name))

    algorithm.fit(data_train1[X_train].copy(), yTR[X_train].copy(), groups=groups[X_train])
    scoresAlgorithm.append(f1_score(yTR[X_test], algorithm.best_estimator_.predict(data_train1[X_test, :]), average="weighted"))
    bestParametersPerFold.append(algorithm.best_estimator_)
    predictionsAlgorithm.append([yTR[X_test], algorithm.best_estimator_.predict(data_train1[X_test, :])])
    cvResults.append(algorithm)

#    cross_val_scoreBest = cross_val_score(algorithm.best_estimator_, data_train1[X_train, ], yTR[X_train], cv=logo, scoring=f1, groups=groups[X_train])
#    cross_val_predictBest = cross_val_predict(algorithm.best_estimator_, data_train1[X_test, :], groups=groups[X_test])

    print("The scores of {} on LOGO cross_val are :".format(name))
    print("\n", scoresAlgorithm)
 #   print("\nThe mean is {}\n The stdDeviation is {}".format(cross_val_scoreBest.mean(), cross_val_scoreBest.std()))
 #   print("The confussion matrix is: \n", confusion_matrix(yTR, cross_val_predictBest))
 #   print("The classification report is: \n", classification_report(yTR, cross_val_predictBest))


    print("\nThe scores of {} on test set :".format(name))
    print("\n F1 score", f1_score(y_true=yTR[X_test], y_pred=algorithm.best_estimator_.predict(data_train1[X_test, :]), average="weighted"))
    print("The confussion matrix is: \n", confusion_matrix(y_true=yTR[X_test], y_pred=algorithm.best_estimator_.predict(data_train1[X_test, :])))
    print("The classification report is: \n", classification_report(y_true=yTR[X_test], y_pred=algorithm.best_estimator_.predict(data_train1[X_test, :])))
    print("\n FINISH TRAINING THE {} MODEL\n".format(name))

    return scoresAlgorithm, bestParametersPerFold, predictionsAlgorithm, algorithm.best_estimator_, cvResults, algorithm
    #return scoresAlgorithm, bestParametersPerFold, predictionsAlgorithm, algorithm.best_estimator_, cross_val_scoreBest, cvResults, algorithm



def model_validation_part(data_train_1, yTR, groups):
    """

    :param data_train_1: This is numpy array containing the data
    :param yTR: This is numpy array containing the targets
    :param groups: This is numpy array containng the groups
    :return:
    """
    from sklearn.decomposition import PCA
    from sklearn.preprocessing import StandardScaler

    from sklearn.ensemble import GradientBoostingClassifier
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.ensemble import AdaBoostClassifier
    from sklearn.model_selection import GridSearchCV
    from sklearn.preprocessing import StandardScaler
    from sklearn.pipeline import Pipeline
    from sklearn.svm import SVC
    from sklearn.multiclass import OneVsRestClassifier

    sc = StandardScaler(copy=True)

    clfRF = OneVsRestClassifier(RandomForestClassifier(class_weight="balanced", random_state=seed, n_jobs=-1))
    clfAB = OneVsRestClassifier(AdaBoostClassifier(random_state=seed))
    clfGB = OneVsRestClassifier(GradientBoostingClassifier(random_state=seed))
    clfSVM = OneVsRestClassifier(SVC(random_state=seed, class_weight="balanced"))



    print("\n Start TRAINING THE Random Forest MODEL\n")

    pipeRF = Pipeline([("standardScaler", sc), ("randomForest", clfRF)])
    randomForestParamGrid = {"randomForest__estimator__max_features":["sqrt", "log2"], "randomForest__estimator__n_estimators":[100]}
    gsRandomForest = GridSearchCV(estimator=pipeRF, param_grid=randomForestParamGrid, cv=logo, scoring=f1, refit=True, verbose=1000,  n_jobs=-1)

    print("\n Start TRAINING THE Ada boost MODEL\n")

    sc = StandardScaler(copy=True)
    pipeAB = Pipeline([("standardScaler", sc), ("adaBoost", clfAB)])
    adaBoostParamGrid = {"adaBoost__estimator__n_estimators":np.arange(50, 1050, 500), "adaBoost__estimator__learning_rate":np.logspace(-1, -1, 1)}
    gsAdaBoost = GridSearchCV(estimator=pipeAB, param_grid=adaBoostParamGrid, cv=logo, scoring=f1, refit=True, verbose=1000,  n_jobs=-1)

    print("\n Start TRAINING THE Gradient Boosting MODEL\n")

    sc = StandardScaler(copy=True)
    pipeGB = Pipeline([("standardScaler", sc), ("gb", clfGB)])
    gbParamGrid = {"gb__estimator__learning_rate":np.logspace(-1,-1, 1), "gb__estimator__n_estimators":np.arange(50, 705, 500), "gb__estimator__max_depth":[1], "gb__estimator__max_features":["sqrt", "log2"]}
    gsBoost = GridSearchCV(estimator=pipeGB, param_grid=gbParamGrid, cv=logo, scoring=f1, refit=True, verbose=1000,  n_jobs=-1)

    print("\n Start TRAINING THE SVM MODEL\n")

    sc = StandardScaler(copy=True)
    pipeSVM = Pipeline([("standardScaler", sc), ("svm", clfSVM)])
    svmParamGrid = [{"svm__estimator__C":np.logspace(-1, -1, 1), "svm__estimator__kernel":["rbf"], "svm__estimator__gamma":np.logspace(-5, -1, 1)},
                    ]
    svmGrid = GridSearchCV(estimator=pipeSVM, param_grid=svmParamGrid, cv=logo, scoring=f1, refit=True, verbose=1000,  n_jobs=-1)

    scoresMethods = []
    SetOfBestEstimators = []
    predictions = []
    bestEstimators = []
    cvScoreBest = []
    cvResultsAll = []
    outerHPOCVresults = []

    from sklearn.model_selection import LeaveOneGroupOut
    logo1 = LeaveOneGroupOut()

    print("\nInput data size ", data_train_1.shape)
    print("\nInput target size ", data_train_1.shape)

    for X_train, X_test in logo1.split(data_train_1, yTR, groups=groups):
        for algorithm, name, col in zip([gsRandomForest, gsAdaBoost, gsBoost,  svmGrid], ["randomForest", "adaBoost", "gradientBoosting", "svm"], ["red", "green", "blue", "black"]):
            #for algorithm, name, col in zip([gsBoost, svmGrid], ["gradientBoosting", "svm"], ["blue", "black"]):
            scoresAlgorithm, bestParametersPerFold, predictionsAlgorithm, algorithm_best_estimator_, cvResults, algorithm = cv_Workflow(algorithm, data_train_1, yTR, name, col, X_train, X_test, groups=groups)
            scoresMethods.append(scoresAlgorithm)
            SetOfBestEstimators.append(bestParametersPerFold)
            bestEstimators.append(algorithm_best_estimator_)
            predictions.append(predictionsAlgorithm)
            #cvScoreBest.append(cross_val_scoreBest)
            cvResultsAll.append(cvResults)
            outerHPOCVresults.append(algorithm)


    return scoresMethods, SetOfBestEstimators, predictions, bestEstimators, cvScoreBest, cvResultsAll, outerHPOCVresults

print("Execution time: ", time()-startTime)